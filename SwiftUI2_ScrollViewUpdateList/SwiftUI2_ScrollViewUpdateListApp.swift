////  SwiftUI2_ScrollViewUpdateListApp.swift
//  SwiftUI2_ScrollViewUpdateList
//
//  Created on 17/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_ScrollViewUpdateListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
